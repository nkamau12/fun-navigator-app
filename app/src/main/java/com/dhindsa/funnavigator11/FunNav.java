package com.dhindsa.funnavigator11;


import android.content.Context;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;


public class FunNav {
    private ArrayList<Path> paths;
    private Path path1;
    private Connect con;
    private double MAX;
    private Mapsi MAP;
    Context context;

    public FunNav(double startlat, double startlon, double max, Context c) {
        paths = new ArrayList<Path>();
        path1 = new Path((long) 0);
        MAX = max;
        context = c;
        MAP = getmap();
        Find(FindNode(startlon, startlat));
    }

    public FunNav(double startlat, double startlon, double max, Mapsi map) {
        paths = new ArrayList<Path>();
        path1 = new Path((long) 0);
        MAP = map;
        MAX = max;
        Find(FindNode(startlon, startlat));
    }

    public boolean Find(Way sofar) {
        if (path1.getLengthEst() > MAX) {
            save();
            return false;
        } else {

            ArrayList<Way> next = FindInt(sofar);
            for (int i = 0; i < next.size() && i < 3; i++) {
                boolean x = false;
                for (int j = 0; j < path1.getintersections().size(); j++) {
                    if (next.get(i).getNode(next.get(i).nodes.size() - 1).getID() == path1.getintersections().get(j).getID()) {
                        x = true;
                        break;
                    }
                }
                if (!x) {
                    path1.addWay(next.get(i));
                    Find(next.get(i));
                    path1.removeLast();
                }

            }
        }
        if (path1.ways.size() > 0) {
            return false;
        } else
            return true;
    }

    private void save() {
        if (paths.size() < 3) {
            paths.add(path1.clone());
        } else {
            if (paths.get(0).getFunRank() < path1.getFunRank()) {
                paths.remove(0);
                paths.add(path1.clone());
            } else if (paths.get(1).getFunRank() < path1.getFunRank()) {
                paths.remove(1);
                paths.add(path1.clone());
            } else if (paths.get(2).getFunRank() < path1.getFunRank()) {
                paths.remove(2);
                paths.add(path1.clone());
            }
        }
    }

    public int nodeCount() {
        int count = 0;
        if (paths.size() == 0)
            return 0;
        for (int i = 0; i < paths.get(0).getNodes().size(); i++)
            count++;
        return count;

    }

    public ArrayList<Node> getPath(int i) {
        return paths.get(i).getNodes();
    }

    public ArrayList<Way> FindInt(Way current) {
        //302316699
        //Utility u = new Utility();
        ArrayList<Way> all = new ArrayList<Way>();
        //System.out.println("Ways "+MAP.getWays().size());
        //System.out.println("Nodes "+MAP.getNodes().size());
        Node pos = current.getNode(current.getSize() - 1);
        //System.out.println(pos.id);
        for (int i = 0; i < MAP.getWays().size(); i++) {
            int pls = 45673821;
            Way path = MAP.getWays().get(i);
            //System.out.println(path.toString());
            Way newpath = new Way(path.getID());
            newpath.addNode(pos);
            for (int j = 0; j < path.getSize(); j++)
                if (path.nodes.get(j).getID() == pos.getID())
                    pls = j;
            if (pls != 45673821) {
                //pls=path.nodes.indexOf(pos);
                //System.out.println("PLS"+pls);
                if (pls == 0 || pls != path.getSize() - 1) {
                    for (int j = pls + 1; j < path.getSize(); j++) {
                        newpath.addNode(path.getNode(j));

                        if (path.getNode(j).isIntersection() && path.getNode(j).getID() != current.getNode(0).getID()) {
                            all.add(newpath.clone());
                            break;
                        }
                    }
                }
                if (pls != 0) {
                    newpath = new Way(path.getID() + 1);
                    newpath.addNode(pos);
                }
                if (pls != 0 || pls == path.getSize() - 1) {
                    for (int j = pls - 1; j >= 0; j--) {
                        newpath.addNode(path.getNode(j));
                        //System.out.println("Intersection: " + path.getNode(j).id + " " + path.getNode(j).isIntersection());
                        if (path.getNode(j).isIntersection() && path.getNode(j).getID() != current.getNode(0).getID()) {
                            all.add(newpath.clone());
                            break;
                        }
                    }
                }
            }

        }
        //System.out.println("num return " + all.size());
        return all;
    }

    public Way FindNode(double lon, double lat) {
        double min = Double.MAX_VALUE;
        Node fin = null;
        for (int i = 0; i < MAP.getNodes().size(); i++) {
            Node n = MAP.getNodes().get(i);
            double x1 = n.getLat();
            double x2 = lat;
            double y1 = n.getLon();
            double y2 = lon;
            double dist = Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
            if (dist < min) {
                min = dist;
                fin = n;
            }
        }
        Way x = new Way(fin.getID());
        x.addNode(fin);

        return x;
    }

    public Mapsi getmap() {
        Mapsi address;

        try {
            InputStream is = context.getAssets().open("copymap.ser");
            //FileInputStream fin = new FileInputStream("copymap.ser");
            ObjectInputStream ois = new ObjectInputStream(is);
            address = (Mapsi) ois.readObject();
            ois.close();

            return address;

        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
