package com.dhindsa.funnavigator11;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.sql.SQLException;


public class MakeFile {

    public MakeFile() throws SQLException {
        MakeFile serial = new MakeFile();
        serial.buildmap(23, 5);

    }

    public void buildmap(double lon, double lat) throws SQLException {
        Connect con = new Connect();
        Mapsi WorkingMap = con.BuildMap(12, 23);
        try {

            FileOutputStream fout = new FileOutputStream("copymap.ser");
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeObject(WorkingMap);
            oos.close();
            System.out.println("Done");

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
