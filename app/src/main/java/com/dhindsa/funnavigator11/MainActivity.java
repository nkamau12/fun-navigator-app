package com.dhindsa.funnavigator11;

import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.google.android.gms.maps.GoogleMap;

import java.io.FileInputStream;
import java.io.ObjectInputStream;


public class MainActivity extends ActionBarActivity {


    GoogleMap googleMap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Globals.mapso =getmap();
        Spinner spinner = (Spinner) findViewById(R.id.steve);
        Spinner spinner2 = (Spinner) findViewById(R.id.spinner);
// Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.planets_array, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        spinner.setAdapter(adapter);
        ArrayAdapter<CharSequence> adapter2= ArrayAdapter.createFromResource(this,
                R.array.distance_array, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        spinner2.setAdapter(adapter2);
        Button btn = (Button) findViewById(R.id.enterbutton);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Spinner mySpinner=(Spinner) findViewById(R.id.steve);
                String text = mySpinner.getSelectedItem().toString();
                Spinner mySpinner2=(Spinner) findViewById(R.id.spinner);
                String text2 = mySpinner2.getSelectedItem().toString();
                String[] parts = text.split(",");
                Intent mapsl = new Intent(MainActivity.this, LoadingScreenActivity.class);
                Bundle b = new Bundle();
                //Bundle c = new Bundle();
                b.putDouble("long", Double.parseDouble(parts[1]));
                b.putDouble("lat", Double.parseDouble(parts[0]));
                b.putInt("dist", Integer.parseInt(text2));
                mapsl.putExtras(b);


                //goMap.putExtra("distance", 10);
                startActivity(mapsl);
            }

        });
    }

    public Mapsi getmap() {
        Mapsi address;
        try {

            AssetManager assets = getAssets();
            AssetFileDescriptor afd = assets.openFd("copymap.wmv");
            FileInputStream fis = afd.createInputStream();
            ObjectInputStream ois = new ObjectInputStream(fis);
            address=(Mapsi)ois.readObject();
            return address;

        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void process(View view) {

    }


}
